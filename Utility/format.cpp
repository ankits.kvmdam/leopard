
void textColorInput()
{
    Box(19, 20, 3, 40);
    printColorList(15);
    cprintf("Press Corresponding Alphabet for     choosing color");
    int op=getInput();
    if(op>90&&op<122) op=op-32;
    op=op-'A';

    if(op>=0&&op<=15) 
    {
        if(cellP[currentRow][currentCol]==NULL)
            allocateMemoryForCellDesign(op,0);
        else
            cellP[currentRow][currentCol]->color=op;
        
        inputSuccessfulColorMessage(op);
    }

    else
        inputInvalidColorMessage();
}

void backgroundColorInput()
{
    Box(19, 20, 3, 40);
    printColorList(7);
    cprintf("Press Corresponding Alphabet for     choosing color");
    int op=getInput();
    if(op>90&&op<122) op=op-32;
    op=op-'A';

    if(op>=0&&op<=7) 
    {
        if(cellP[currentRow][currentCol]==NULL)
            allocateMemoryForCellDesign(15,op);
        else
            cellP[currentRow][currentCol]->background=op;
        
        inputSuccessfulColorMessage(op);
    }

      else
        inputInvalidColorMessage();

    
}