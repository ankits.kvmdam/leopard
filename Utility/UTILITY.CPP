/*get column name like AA or AZ*/
void getColName(char *s, int colNumber)
{
    int i=0;
    int rem;
    while(colNumber>0)
    {
	rem=(colNumber-1)%26;
	s[i++]=65+rem;
	colNumber=(colNumber-rem)/26;
    }
    s[i]='\0';
    strrev(s);
}

void insertElement(char *s, int len, int pos, char c)
{
    int i;
    for(i=len-1;i>=pos-1;i--)
        s[i+1]=s[i];
    s[pos-1]=c;
    s[len+1]='\0';
}

void deleteElement(char *s, int len, int pos)
{
    int i;
    if(len==pos-1) 
    {
        s[len-1]='\0';
        return;
    }

    for(i=pos-2;i<len;i++)
        s[i]=s[i+1];
    s[len-1]='\0';
}


int compareString(char *source, char *check, int start, int end)
{
    int i,j,c;
    for(i=start,j=0;i<end;i++,j++)
    {
        c=source[i];
        if(c>90) 
            c=c-32;

        if(c!=check[j])
            return FALSE;
    }
    if(check[j]!='\0') 
        return FALSE;
    return TRUE;
}

float stringToFloat(char *s)
{
    float num;
    sscanf(s,"%f",&num);
    return num;
}

void printCellData(int row, int col)
{
    if(cell[row][col]->dataType==STRING)
            cprintf("%s",cell[row][col]->u.text);
        else if(cell[row][col]->dataType==VALUE)
            cprintf("%.2f",cell[row][col]->u.value);
        else if(cell[row][col]->dataType==EXPRESSION)
            cprintf("%lf",cell[row][col]->u.e.expressionValue);
}


void saveData()
{
    Box(3,11,9,60,BLUE);
    gotoxy(2,1);
    cprintf("File Name (Max Length 10): ");
    gotoxy(50,1);
    setColorBackground(WHITE,BLUE);
    cprintf(" SAVE ");
    gotoxy(2,3);
    setColorBackground(RED,WHITE);
    cprintf("*Press ESC to go back");
    int check=takeData(10,41,10,60,10);
    if(check==TRUE)
    {
        writeDataOnFile();
        printTitle();
    }
    
}

int checkColRowValue(char *str, int start, int end)
{
    int row, col, i;
    for(i=start;i<end;i++)
        if(isdigit(str[i]))
            break;

    col=getColValue(str, start, i);
    row=getRowValue(str, i, end);
    
    if(col<=MAXCOL&&row<=MAXROW)
        return TRUE;
    return FALSE;
}

int getRowValue(char *str, int start, int end)
{
    char temp[6];
    int i,j;
    for(i=start,j=0;i<end;i++)
        temp[j++]=str[i];
    temp[j]='\0';

    sscanf(temp,"%d",&j);
    return j;
}

int getColValue(char *str, int start, int end)
{
    int i,j;
    for(i=start,j=0;i<end;i++)
    {
        if(isdigit(str[i])) break;
        char c=str[i];
        c=toupper(c);
        j=(j*26)+(c-'A'+1);
    }
    return j;
}


/*int reduce(char *str, int start, int mid, int end)
{

}
/*
void reduceExpression(char *str)
{
    int i,op=FALSE;
    for(i=0;str[i]!='\0';i++)
    {
        op=checkOperator(str[i]);
        if(op==TRUE&&str[i]!=')')
        {
            if(str[i+2]=='(')
                continue;
            else if(str[i+2]=='+'||str[i+2]=='-')
                i=reduce(str,i+2);
        }

    }
}

int solveExpression(char *str)
{
    char modifiedExpression[85];
    epressionSeparator(str, modifiedExpression);
    //reduceExpression(str);
    cout<<modifiedExpression;
    getch();
}*/

void copyCell()
{
    if(cell[currentRow][currentCol]==NULL)
        printStatus("EMPTY CELL", RED, WHITE);
    
    else
    {
        copied=TRUE;
        if(cell[currentRow][currentCol]->dataType==STRING)
        {
            copiedCellRecord.dataType=STRING;
            strcpy(copiedCellRecord.u.text,cell[currentRow][currentCol]->u.text);
        }
        else if(cell[currentRow][currentCol]->dataType==VALUE)
        {
            copiedCellRecord.dataType=VALUE;
            copiedCellRecord.u.value=cell[currentRow][currentCol]->u.value;
        }
        else if(cell[currentRow][currentCol]->dataType==EXPRESSION)
        {
            copiedCellRecord.dataType=EXPRESSION;
            strcpy(copiedCellRecord.u.e.expression,cell[currentRow][currentCol]->u.e.expression);
            copiedCellRecord.u.e.expressionValue=cell[currentRow][currentCol]->u.e.expressionValue;
            copiedCellRecord.u.e.status=cell[currentRow][currentCol]->u.e.status;
        }
        printStatus(" Copied",BLUE);
    }
}

void pasteCell()
{
    if(copied==FALSE)
	printStatus("Nothing to paste",RED);

    else if(copied==TRUE)
    {
	if(copiedCellRecord.dataType==STRING)
	    allocateMemoryForText(copiedCellRecord.u.text);
	else if(copiedCellRecord.dataType==VALUE)
	    allocateMemroyForValue(copiedCellRecord.u.value);
	else if(copiedCellRecord.dataType==EXPRESSION)
	    allocateMemroyForExpression(
		copiedCellRecord.u.e.expression,
		copiedCellRecord.u.e.expressionValue,
		copiedCellRecord.u.e.status
	    );
        printStatus(" Paste", BLUE);
    }
}

void cutCell()
{
    if(cell[currentRow][currentCol]!=NULL)
    {
	copyCell();
    deleteCell();
    copied=TRUE;
    printStatus(" Cut", BLUE);
    }
}