#include "..\SOURCE\LEOPARD\Header\leopard.h"
#include "..\SOURCE\LEOPARD\GUI\Display\display.cpp"
#include "..\SOURCE\LEOPARD\Utility\setter.cpp"
#include "..\SOURCE\LEOPARD\Utility\utility.cpp"
#include "..\SOURCE\LEOPARD\GUI\Cursor\cursor.cpp"
#include "..\SOURCE\LEOPARD\GUI\Scroll\scroll.cpp"
#include "..\SOURCE\LEOPARD\Input\input.cpp"
#include "..\SOURCE\LEOPARD\Input\check.cpp"
#include "..\SOURCE\LEOPARD\Option\option.cpp"
#include "..\SOURCE\LEOPARD\Memory\alloc.cpp"
#include "..\SOURCE\LEOPARD\Utility\format.cpp"

void process()
{
    int ch;
    do{

	ch=getInput();
	printStatus(READY);
	switch(ch)
	{
	    case UP: moveCursorUp();break;
	    case DOWN: moveCursorDown(); break;
	    case RIGHT: moveCursorRight();break;
	    case LEFT: moveCursorLeft(); break;
	    case PAGEDOWN: pageDown(); break;
	    case PAGEUP: pageUp(); break;
	    case CTRLLEFT: ctrlLeft(); break;
	    case CTRLRIGHT: ctrlRight(); break;
	    case DEL: deleteCell();updateCursor(PREV,ACTIVE);break;
	    case F1: f1Option(); printWholeScreen(); break;
	    case F2: editOption(); printWholeScreen(); break;
	    case F3: formatOption();printWholeScreen(); break;
	    case CTRLC: copyCell();break;
	    case CTRLV: pasteCell();updateCursor(PREV,ACTIVE);break;
	    case CTRLX: cutCell(); updateCursor(PREV,ACTIVE);break;
		default:
		if(ch>=32&&ch<=126)
		inputHandler(ch);
	}
      }while(TRUE);
}


void main()
{
currentX=currentY=currentCol=currentRow=1;
_setcursortype(0);
setColorBackground(WHITE,BLACK);
clrscr();
printDialogBox("Welcome to Leopard. It is developed by Ank.");
getch();
printWholeScreen();
process();
}