int getInput()
{
    int key, hi, lo;
    key=bioskey(0);
    hi=(key & 0xff00)>>8;
    lo=key & 0x00ff;
    return ((lo==0)?hi+256:lo);
}

void takeInput(char *str, int len)
{
    int c, xpos, checkReached;
    xpos=len+1;
    do{
        clearDisplayAndPrint(str, xpos, 1);
        c=getInput();
        switch(c)
        {
            case LEFT: if(xpos>1)
                            xpos--;
                         break;

            case RIGHT: if(xpos<len+1)
                            xpos++;
                        break;

            case BS: if(xpos>1)
                        deleteElement(str, len--, xpos--);
                     break;

            case DEL:   if(xpos<=len)
                            deleteElement(str, len--, xpos+1);
                        break;
           
            default:    if(c>=32&&c<=126&&len<40) 
                            {
                                if(xpos-1==len)
                                {
                                    str[len++]=c; 
                                    str[len]='\0';
                                }
                                else 
                                    insertElement(str, len++, xpos, c); 
                                xpos++;
                            }

        }
        
        if(len==40)
         {
            setStatus(MAXLENREACH, 11, 24, 65, 24, RED, WHITE);
            checkReached=1;
          }
          else if (checkReached==1)
          {
            setStatus(READY, 11, 24, 65, 24);
            checkReached=0;
          }
    }while(c!=13);
}


void inputHandler(int ch)
{
    char str[40];
    str[0]=ch;
    str[1]='\0';
    int len=1, checkType;

    setWindow(11,24,65,24);
    _setcursortype(2);
    takeInput(str, len);
    _setcursortype(0);

    checkType=checkInput(str);
    printStatus(READY);

    if(checkType==EXPRESSION)
    {
        int check=checkExpression(str);
        if(check==TRUE)
        {
           // solveExpression(str);
            allocateMemroyForExpression(str,checkExpression(str),1);
        }
    }
    else if(checkType==STRING) allocateMemoryForText(str);
    else if(checkType==VALUE) allocateMemroyForValue(stringToFloat(str));



    updateCursor(PREV,ACTIVE);
   // cout<<str;
}

int takeData(int maxLen, int x1, int y1, int x2, int y2)
{
    _setcursortype(2);
    char temp[12];
    setWindow(x1-1,y1,x2,y2,RED);
    setWindow(x1,y1,x2,y2,RED);
    strcpy(temp,"");
    int len=0,xpos,c;
    xpos=len+1;


    do{
        clearDisplayAndPrint(temp, xpos, 1);
        c=getInput();
        switch(c)
        {
            case LEFT: if(xpos>1)
                            xpos--;
                         break;

            case RIGHT: if(xpos<len+1)
                            xpos++;
                        break;

            case BS: if(xpos>1)
                        deleteElement(temp, len--, xpos--);
                     break;

            case DEL:   if(xpos<=len)
                            deleteElement(temp, len--, xpos+1);
                        break;
                        
            case ESC:   _setcursortype(0);
                        return FALSE;
           
            default:    if(c>=32&&c<=126&&len<maxLen) 
                            {
                                if(xpos-1==len)
                                {
                                    temp[len++]=c; 
                                    temp[len]='\0';
                                }
                                else 
                                    insertElement(temp, len++, xpos, c); 
                                xpos++;
                            }

        }
    
    }while(c!=13);
    temp[len]='\0';
    _setcursortype(0);
    strcpy(fileName,temp);
    return TRUE;
}

