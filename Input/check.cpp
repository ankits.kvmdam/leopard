int checkInput(char *s)
{
    int i, countDecimal=0;
    if(s[0]=='=') return EXPRESSION;
    else if(s[0]=='\'') return STRING;

    for(i=0;s[i]!='\0';i++)
    {
        if((s[i]<'0'||s[i]>'9')&&s[i]!='.') return STRING;
        if(s[i]=='.') countDecimal++;
        if(countDecimal>1) return STRING;
    }
    
    return VALUE;
}


void epressionSeparator(char *source, char *dest)
{
    int i,j, lastNum=FALSE;
    for(i=1,j=0;source[i]!='\0';i++)
    {
        if(isalpha(source[i])||isdigit(source[i])||source[i]=='.')
        {
            dest[j++]=source[i];
            lastNum=TRUE;
        }
        else
        {
            if(lastNum==TRUE)
            {
                lastNum=FALSE;
                dest[j++]=',';
            }
            dest[j++]=source[i];
            dest[j++]=',';
        }

    }
    dest[j]='\0';
}

int checkSyntax(char *s)
{
   int i;
   int plusMinus=FALSE, divideMul=FALSE, openBracket=FALSE, closeBracket=FALSE;
   int op=FALSE, openBracketNeccessary=FALSE;
   int countBracket=0;

   for(i=0;s[i]!='\0';i++)
   {
       
        if(s[i]==',')
            continue;
        
        else if(s[i]=='.')
            continue;

        else if(isdigit(s[i])&&closeBracket==TRUE)
            return FALSE;
        
        else if(isdigit(s[i]))
        {
            op=TRUE;
            openBracket=plusMinus=divideMul=FALSE;
            continue;
        }
  
       else if(isalpha(s[i]))
       {
           int j,c;
           op=TRUE;
           openBracket=plusMinus=divideMul=FALSE;
           for(j=i;s[j]!=',';j++);
           
           c=checkFunctionName(s,i,j);
           if(c==TRUE)
           {
               if(s[j+3]=='\0') return FALSE;
               i=j;
               openBracketNeccessary=TRUE;
               op=FALSE;
               continue;
           }
       }
       switch(s[i])
       {
           case '+':
           case '-':
                    if(s[i+2]==')'||openBracketNeccessary==TRUE) return FALSE;
                    plusMinus=TRUE;
                    op=FALSE;
                    openBracket=FALSE;
                    closeBracket=FALSE;
                    
                    break;

           case '*':
           case '/':
           case '^':
                    if(plusMinus==TRUE||openBracket==TRUE||divideMul==TRUE||s[i+4]=='\0'||openBracketNeccessary==TRUE)
                        return FALSE;
                    divideMul=TRUE;
                    op=FALSE;
                    closeBracket=FALSE;
                    break;
                    
           case '(':
                    if(op==TRUE) return FALSE;
                    if(closeBracket==TRUE) return FALSE;
                    divideMul=FALSE;
                    openBracketNeccessary=FALSE;
                    countBracket++;
                    openBracket=TRUE;
                    break;

           case ')':
                    countBracket--;
                    if(countBracket<0) return FALSE;
                    closeBracket=TRUE;
       }
   }
   if(countBracket==0) return TRUE;
   return FALSE;
}


int checkOperator(char *s, int pos)
{
    if(
        s[pos]=='+'||
        s[pos]=='-'||
        s[pos]=='*'||
        s[pos]=='/'||
        s[pos]=='('||
        s[pos]==')'||
        s[pos]=='^'||
        s[pos]=='%'
        )
        return TRUE;
    return FALSE;

}

int checkNumber(char *s, int start, int end)
{
    int i;
    int countDecimal=0;
    for(i=start;i<end;i++)
    {
        if(s[i]=='.')
        {
            countDecimal++;
            if(countDecimal==2)
                return FALSE;
        }

        else if(s[i]<'0'||s[i]>'9')
            return FALSE;
    }
    return TRUE;
}

int checkCellName(char *s, int start, int end)
{
    if((end-start)>5) return FALSE;
    if(!isalpha(s[start])) return FALSE;
    int noAlpha=0, i;
    for(i=start+1;i<end;i++)
    {
        if(!isdigit(s[i])&&!isalpha(s[i])) return FALSE;

        else if(isdigit(s[i]))
        noAlpha=1;
        else if(isalpha(s[i])&&noAlpha==1) return FALSE;
    }

    return checkColRowValue(s,start,end);
}

int checkFunctionName(char *s, int start, int end)
{
    if(
        compareString(s,"TAN",start,end)||
        compareString(s,"COS",start,end)||
        compareString(s,"SIN",start,end)||
        compareString(s,"ASIN",start,end)||
        compareString(s,"ACOS",start,end)||
        compareString(s,"ATAN",start,end)||
        compareString(s,"LOG",start,end)||
        compareString(s,"LOG10",start,end)||
        compareString(s,"SQRT",start,end)
    )
        return TRUE;
    return FALSE;
}

int checkIt(char *s, int start, int end)
{
    if(checkOperator(s,start))
        return TRUE;
    else if(checkCellName(s,start,end))
        return TRUE;
    else if(checkNumber(s,start,end))
        return TRUE;
    else if(checkFunctionName(s,start,end))
        return TRUE;

    return FALSE;
}


int checkInputData(char *s)
{
    int i,j,check;
    for(i=0,j=0;s[i]!='\0';i++)
    {
        if(s[i]==',')
        {
            check=checkIt(s,j,i);
            j=i+1;
            if(check==FALSE) return FALSE;
        }

    }
    return TRUE;
}

int checkExpression(char *str)
{
    char modifiedExpression[85];
    int check,len=strlen(str);
    insertElement(str, len, 2, '(');
    len++;
    str[len++]=')';
    str[len]='\0';
    epressionSeparator(str, modifiedExpression);
    check=checkInputData(modifiedExpression);
    if(check==TRUE)
        check=checkSyntax(modifiedExpression);
    return check;

}