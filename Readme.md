# Leopard v1.0

This is a simple spreedsheet in c++, currently in developement.

## To run

### Requirements

* Windows Operating System
* Turbo C++ Compiler [[Download](https://www.softpedia.com/get/Programming/Coding-languages-Compilers/TurboCplusplus-for-Windows-7.shtml)]

### Steps 

* First you have to download and install the Turbo C++ Compiler
* Clone this repo to SOURCE folder in the directory created by Turbo C++. Usually C:\TurboC++\Disk\TurboC3\SOURCE.
* Open LEOPARD.CPP in Turbo C++.
* Compile. [Don't change the default setting and options of turbo c++.]

### Example Screen

![Welcome Screen](./Example/welcome.png)
![Starting Screen](./Example/starting.png)
![Option Screen](./Example/Options.png)
![Entries Screen](./Example/Entries.png)
![Edit Screen](./Example/edit.png)