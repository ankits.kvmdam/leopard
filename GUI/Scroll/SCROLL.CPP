void scrollDown()
{
    if(currentRow<100)
    {
	currentRow++;
	printRow(currentRow-ROW+1);
    displaySheetCursor();
    }
}

void scrollUp()
{
    if(currentRow>1)
    {
	currentRow--;
	printRow(currentRow);
    displaySheetCursor();
    }
}

void scrollLeft()
{
    if(currentCol>1)
    {
	currentCol--;
	printCol(currentCol);
    displaySheetCursor();
    }
}

void scrollRight()
{
    if(currentCol<100)
    {
	currentCol++;
	int i,c=defaultColWidth;
	for(i=0;;i++)
	 {
	    c+=defaultColWidth;
	    if(c+defaultColWidth>71) break;
	 }

	 printCol(currentCol-i-1);
     displaySheetCursor();
    }
}


void pageDown()
{
    if(currentY<ROW)
    {
	cursorUpdateSetter(PREV, ROW, DEACTIVE);
	currentY=ROW;
	currentRow=lastRow;
	cursorUpdateSetter(PREV, ROW, ACTIVE);
    displaySheetCursor();
    }
    else if(currentRow+ROW<=100)
    {
	cursorUpdateSetter(PREV, ROW, DEACTIVE);
	currentY=1;
	currentRow++;
	printRow(currentRow);
	cursorUpdateSetter(PREV, ROW, ACTIVE);
    displaySheetCursor();
    }
    else
    {
    cursorUpdateSetter(PREV, ROW, DEACTIVE);
    currentY=currentRow-80;
    printRow(81);
    cursorUpdateSetter(PREV, ROW, ACTIVE);
    displaySheetCursor();
    }

}


void pageUp()
{
    if(currentY>1)
    {
    cursorUpdateSetter(PREV, ROW, DEACTIVE);
	currentY=1;
	currentRow=firstRow;
	cursorUpdateSetter(PREV, ROW, ACTIVE);
    displaySheetCursor();
    }

    else if(currentRow-ROW>=1)
    {
    cursorUpdateSetter(PREV, ROW, DEACTIVE);
	currentY=ROW;
	currentRow--;
	printRow(currentRow-ROW+1);
	cursorUpdateSetter(PREV, ROW, ACTIVE);
    displaySheetCursor();
    }

    else
    {
    cursorUpdateSetter(PREV, ROW, DEACTIVE);
    currentY=currentRow;
    printRow(1);
    cursorUpdateSetter(PREV, ROW, ACTIVE);
    displaySheetCursor();
    }
}


void ctrlRight()
{
    if(currentCol!=lastCol)
    {
	cursorUpdateSetter(PREV, COL, DEACTIVE);
	currentX=lastColX+1;
	currentCol=lastCol;
	cursorUpdateSetter(PREV, COL, ACTIVE);
    displaySheetCursor();
    }
    else if(currentCol+2<MAXCOL)
    {
    cursorUpdateSetter(PREV, COL, DEACTIVE);
    currentX=1;
    currentCol++;
    printCol(currentCol);
    cursorUpdateSetter(PREV, COL, ACTIVE);
    displaySheetCursor();
    }
}

void ctrlLeft()
{

if(currentCol!=firstCol)
{
cursorUpdateSetter(PREV, COL, DEACTIVE);
currentX=1;
currentCol=firstCol;
cursorUpdateSetter(PREV, COL, ACTIVE);
displaySheetCursor();
}

else if(currentCol-5>1)
{
cursorUpdateSetter(PREV, COL, DEACTIVE);
currentCol--;

int i,c=defaultColWidth;
	for(i=0;;i++)
	 {
	    c+=defaultColWidth;
	    if(c+defaultColWidth>71) break;
	 }

	 printCol(currentCol-i-1);

currentX=lastColX;
cursorUpdateSetter(PREV, COL, ACTIVE);
displaySheetCursor();
}
}
