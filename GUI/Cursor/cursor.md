#cursor.cpp

-void updateIndicator(int rowCol, int activeDeactive)
This function updates the row and col indicator.
First argument is for asking which thing to update.
Second argument is for asking whether to update as activated or deactivated.

-void updateCursor(int prevWhole, int activeDeactive)
This function is for update the cursor.
First argurment is meaning less because it didn't use till now.
Second argument is for removing the cursor from previous location
and update the current location.

-void moveCursorRight()
This function is to move cursor right.

-void moveCursorLeft()
This function is to move cursor left.

-void moveCursorDown()
This function is to move cursor down.

-void moveCursorUp()
This function is to move cursor up.

-void updateMenuCursor(int pos, char option[][10], int status)
This function is to just update the menu cursor.
First argument is for which position, developer want to update cursor.
Second argument is for the list of options available.
Third argument is for asking whether current position is active or deactive.

