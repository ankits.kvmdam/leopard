
void allocateMemoryForText(char *s)
{
    cellPointer temp;
    temp=(cellPointer)(malloc(strlen(s)+2));
    temp->dataType=STRING;
    strcpy(temp->u.text,s);
    cell[currentRow][currentCol]=temp;
}

void allocateMemroyForValue(float value)
{
    cellPointer temp;
    temp=(cellPointer)(malloc(sizeof(float)+1));
    temp->u.value=value;
    temp->dataType=VALUE;
    cell[currentRow][currentCol]=temp;
}

void allocateMemroyForExpression(char *s, float value, int status)
{
    cellPointer temp;
    temp=(cellPointer)(malloc(sizeof(float)+strlen(s)+sizeof(int)+3));
    temp->dataType=EXPRESSION;
    temp->u.e.expressionValue=value;
    strcpy(temp->u.e.expression,s);
    temp->u.e.status=status;
    cell[currentRow][currentCol]=temp;
}

void allocateMemoryForCellDesign(int color, int background)
{
    cellProperty temp;
    temp=(cellProperty)(malloc(sizeof(int)*2));
    temp->color=color;
    temp->background=background;
    cellP[currentRow][currentCol]=temp;
}


void deleteCell()
{
    free(cell[currentRow][currentCol]);
    cell[currentRow][currentCol]=NULL;
    if(cellP[currentRow][currentCol]!=NULL)
        deleteCellProperty();
}

void deleteCellProperty()
{
    free(cellP[currentRow][currentCol]);
    cellP[currentRow][currentCol]=NULL;
}

int writeDataOnFile()
{
    char temp[15];
    strcpy(temp,fileName);
    strcat(temp,".ALP");
    ofstream fout(temp,ios::out|ios::binary);
    int i,j;
    for(i=1;i<=100;i++)
    {
        for(j=1;j<=100;j++)
        {
            if(cell[i][j]!=NULL)
            {
                cellxy.x=i;
                cellxy.y=j;
                fout.write((char *)&cellxy,sizeof(cellxy));
                fout.write((char *)cell[i][j],sizeof(struct cellRecord));
            }
        }
    }
    fout.close();

} 

/*int openFile()
{
    ifstream fin("TEST1.ALP",ios::binary);
}*/