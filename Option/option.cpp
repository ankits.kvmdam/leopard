
int controler(int length, char option[][10])
{
    int ch,op=0;
    do{
        updateMenuCursor(op, option,ACTIVE);
        ch=getInput();
        switch(ch)
        {
            case DOWN: 
                    updateMenuCursor(op, option, DEACTIVE);
                    op=(++op)%length;
                    break;
            case UP:
                    updateMenuCursor(op, option, DEACTIVE);
                    if(op!=0)
                    op=(--op)%length;
                    else op=length-1;
                    break;
            case ENTER:
                return op;
        }
        
    }while(ch!=27);

    return -1;
}


void f1Option()
{
    setWindow(1,2,14,2,RED,WHITE);
    cprintf(" %s",menu[0]);
    char option[][10]={"New", "Open", "Save", "Save as","Close"};
    Box(5,1,3);
    printOptions(option,5);
    int ch,op=0;
    op=controler(5,option);
    switch(op)
    {
        case 2: saveData();
                break;
        case 4: exit(0);
    }

    }

void editOption()
{
    setWindow(15,2,28,2,RED,WHITE);
    cprintf(" %s",menu[1]);
    char option[][10]={"Cut", "Copy", "Paste", "Delete"};
    Box(4,15,3);
    printOptions(option,4);
    int ch,op=0;
    op=controler(4,option);

    switch(op)
    {
        case 0: copyCell(); deleteCell(); break;
        case 1: copyCell();break;
        case 2: pasteCell(); break;
        case 3: deleteCell();break;
    }
}

void formatOption()
{
    int op=0;
	setWindow(29,2,41,2,RED,WHITE);
	cprintf(" %s",menu[2]);
	char option[][10]={"Backcolor", "TextColor"};
	Box(2,28,3);
	printOptions(option,2);
	op=controler(2,option);
	switch(op)
	{
	    case 0: backgroundColorInput();
		    break;
	    case 1: textColorInput();
		    break;
	}

}
